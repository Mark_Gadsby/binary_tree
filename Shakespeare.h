#include "BinaryTree.h"
#include <string>

class WorksOfShakespeare
{
    private:
        BinaryTree* pBinaryTree; 
        string* pArray; 

    public:
        WorksOfShakespeare();
        ~WorksOfShakespeare();

        int FindInTree(string item_sought);
        void BuildTreeFromWorks();
        void BuildArrayFromTree();
        int GetNodeCount();
        void Display();
        void PrintTreeToFile();
        string* GetArray();
};